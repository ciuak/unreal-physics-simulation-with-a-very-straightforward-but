class Vector(private var _x : Double, private var _y : Double) extends Cloneable {
  def this(args : Double*) = this(args(0), args(1))
  def this(_len : Double, _theta : Double, _radians : Boolean) =
    this(Vector.args_for(_len, _theta, _radians) : _*)
  override def clone = new Vector(_x, _y)

  def asP = new Position(_x, _y)

  def negate = new Vector(-_x, -_y)
  def +=(v : Vector) {
    _x += v.x
    _y += v.y
  }
  def *(i : Double) = new Vector(x * i, y * i)
  def -=(v : Vector) { this += v.negate }
  def x = _x
  def x_=(value : Double) { _x = value }
  def y = _y
  def y_=(value : Double) { _y = value }
  def len = Math.sqrt(_x*_x + _y*_y)
  def len_=(value : Double) {
    _x = value * Math.cos(theta)
    _y = value * Math.sin(theta)
  }
  def theta = Math.atan2(_y, _x)
  def theta_deg = theta * 180 / Math.PI
  def theta_=(value : Double) {
    _x = len * Math.cos(value)
    _y = len * Math.sin(value)
  }
  def theta_deg_=(value : Double) {
    theta_=(value * Math.PI / 180)
  }
}

object Vector {
  private def args_for(len : Double, theta : Double, radians : Boolean) = {
    val radian_angle = theta * (if(radians) { 1D } else { Math.PI / 180 })
    Array(Math.cos(radian_angle) * len,
      Math.sin(radian_angle) * len)
  }
}