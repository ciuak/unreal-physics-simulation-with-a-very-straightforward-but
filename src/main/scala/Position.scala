class Position(var x : Double, var y : Double) extends Cloneable {
  override def clone() = new Position(x, y)
  def +(v : Vector) =
    new Position(x + v.x, y + v.y)
  def -(v : Vector) =
    new Position(x - v.x, y - v.y)
  def asV = new Vector(x, y)
  def +=(v : Vector) {
    x += v.x
    y += v.y
  }
  def -=(v : Vector) {
    x -= v.x
    y -= v.y
  }
  def dist(p : Position) : Vector =
    (this - p.asV).asV
}
