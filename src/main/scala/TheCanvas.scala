import java.awt._
import javax.swing.JPanel

class TheCanvas extends JPanel {
  def dim = (getWidth, getHeight)
  override def paintComponent(g : Graphics) = {
    val g2d = g.create.asInstanceOf[Graphics2D]
    super.paintComponent(g2d)
    g2d.fillRect(0, 0, dim._1, dim._2)

    Controller.entities.foreach { (e : Entity) =>
      val local = g2d.create.asInstanceOf[Graphics2D]
      local.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON)
      local.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
        RenderingHints.VALUE_INTERPOLATION_BILINEAR)

      if(e.alive)
        e.draw(local)

      /* e match {
        case withSpeed : EntityWithSpeed =>
          local.setColor(Color.BLACK)
          local.setFont(Font.decode("Monospaced 12"))
          local.drawString(withSpeed.speed.len.toString,
            withSpeed.position.x.asInstanceOf[Int], withSpeed.position.y.asInstanceOf[Int])
        case _ =>
      } */



      e match {
        case e : EntityWithSpeed =>
          local.setColor(Color.GREEN)
          local.drawLine(
             e.position.x                  .asInstanceOf[Int],
             e.position.y                  .asInstanceOf[Int],
            (e.position.x + e.speed.x * 50).asInstanceOf[Int],
            (e.position.y + e.speed.y * 50).asInstanceOf[Int])
        case _ =>
      }



      local.dispose()
    }

    /*
    val length = Controller.entities.map({
      case c : ObjectConnection => c.length
      case _ => 0.0
    }).sum.asInstanceOf[Int]
    g2d.setFont(Font.decode("Monospace 36"))
    g2d.setColor(Color.GRAY)
    g2d.drawString(length.toString, width/2, height/2)
    */

    g2d.dispose()
  }
}
