trait EntityWithSpeed extends EntityWithPosition {
  var speed = new Vector(0, 0)
  def tick() {
    position += speed
  }
}
