import java.awt.Graphics2D

trait Entity {
  var alive : Boolean = false
  def draw(g : Graphics2D)
  def tick()
}
