import java.awt.{Color, Graphics2D}

class ObjectConnection(val nodes : (EntityWithSpeed, EntityWithSpeed),
                       val distance : Double,
                       val power : Double) extends LivingEntity {

  def length() = {
    nodes._1.position.dist(nodes._2.position).len
  }

  override def draw(g : Graphics2D) {
    g.setColor(Color.LIGHT_GRAY)
    g.drawLine(
      nodes._1.position.x.asInstanceOf[Int],
      nodes._1.position.y.asInstanceOf[Int],
      nodes._2.position.x.asInstanceOf[Int],
      nodes._2.position.y.asInstanceOf[Int])
  }
  override def tick() {
    val vec = nodes._1.position.dist(nodes._2.position)
    val dist = length()
    val amplifier = power * (dist / distance - 1)
    val angle = vec.theta
    val force = new Vector(amplifier * Math.cos(angle),
                           amplifier * Math.sin(angle))
    nodes._1.speed = (nodes._1.speed.asP - force).asV
    nodes._2.speed = (nodes._2.speed.asP + force).asV
  }
}
