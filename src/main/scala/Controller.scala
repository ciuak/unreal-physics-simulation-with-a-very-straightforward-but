import java.awt.{Color, Font, Dimension}
import java.awt.event.{MouseAdapter, MouseEvent}
import java.util
import javax.swing._
import javax.swing.event.{DocumentEvent, DocumentListener}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Controller {
  val repel_constant = 1
  var mouse_location = (0, 0)
  var slider_value = 0
  var mouse_pressed = false
  var ticks = 0
  val maximum_value = 19
  val canvas = new TheCanvas
  var entities_no = 0
  var entity_field_correct = true
  def reload_entities(e_no : Int) {
    entities_no = e_no
    entities = Array.tabulate(entities_no) { (i : Int) =>
      new Sphere(
        new Position(canvas.dim._1 * (0.5 + 0.35 * Math.cos(i * Math.PI * 2 / entities_no)),
          canvas.dim._2 * (0.5 + 0.35 * Math.sin(i * Math.PI * 2 / entities_no))),
        17, (0, 0, canvas.dim._1, canvas.dim._2), new Vector(0, 0))
    }
    entities = entities ++ List.tabulate(entities_no) { (i : Int) =>
      new ObjectConnection(
        (entities(i % entities_no).asInstanceOf[Sphere],
          entities((i + 1) % entities_no).asInstanceOf[Sphere]), 25, 0.005)
    }
  }
  var entities  = Array[Entity]()
  var entities_ = Array[Entity]()
  val simulation_frame = new JFrame
    simulation_frame.add(canvas)
    simulation_frame.pack()
    simulation_frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE)
    simulation_frame.setLocationRelativeTo(null)
    simulation_frame.setTitle("simulation")
    simulation_frame.setResizable(true)
    simulation_frame.addMouseListener(new MouseAdapter {
      override def mousePressed(e : MouseEvent) {
        mouse_pressed = true
      }
      override def mouseDragged(e : MouseEvent) {
        mouseMoved(e)
      }
      override def mouseMoved(e : MouseEvent) {
        mouse_location = (e.getX, e.getY)
      }
      override def mouseReleased(e : MouseEvent) {
        mouse_pressed = false
      }
    })
  val slider = new JSlider(SwingConstants.VERTICAL, -1, maximum_value + 1, -1)
    slider.setSnapToTicks(true)
    slider.setMinorTickSpacing(1)
    slider.setPaintTicks(true)
    slider.setPaintLabels(true)
    slider.setPreferredSize(new Dimension(30, 240))
    val label_table = new util.Hashtable[Integer, JLabel]()
      label_table.put(-1, new JLabel("0 t/s"))
      for(i <- 0 to maximum_value)
        label_table.put(i, new JLabel(neat_value(exp_value(i))))
      label_table.put(maximum_value + 1, new JLabel("\u221E"))
    slider.setLabelTable(label_table)
  val entity_box = new JTextField()
    entity_box.setFont(Font.decode("Monospaced 12"))
    entity_box.setText("5")
    entity_box.getDocument.addDocumentListener(new DocumentListener {
      override def insertUpdate(e : DocumentEvent) {
        on_edit(e)
      }
      override def changedUpdate(e : DocumentEvent) {
        on_edit(e)
      }
      override def removeUpdate(e : DocumentEvent) {
        on_edit(e)
      }
      def on_edit(e : DocumentEvent) {
        entity_field_correct = "^(0|[1-9][0-9]*)$".r.findFirstIn(entity_box.getText).isDefined
        reset_button.setEnabled(entity_field_correct)
        if(entity_field_correct)
          entity_box.setForeground(Color.BLACK)
        else
          entity_box.setForeground(Color.RED)
      }
    })
  val reset_button = new JButton("Reset")
    reset_button.setEnabled(true)
    reset_button.addMouseListener(new MouseAdapter {
      override def mousePressed(e : MouseEvent) {
        if(entity_field_correct)
          reload_entities(entity_box.getText.toInt)
      }
    })
  val quit_button = new JButton("Quit")
    quit_button.addMouseListener(new MouseAdapter {
      override def mousePressed(e : MouseEvent) {
        Runtime.getRuntime.exit(0)
      }
    })
  val controller = new JPanel
    controller.add(slider)
    controller.add(entity_box)
    controller.add(reset_button)
    controller.add(quit_button)
    controller.setLayout(new BoxLayout(controller, BoxLayout.Y_AXIS))
  val controller_frame = new JFrame
    controller_frame.add(controller)
    controller_frame.setTitle("controller")
    controller_frame.setResizable(false)
    controller_frame.pack()
    controller_frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE)
    controller_frame.setLocation(0, 0)
  def run() {
    simulation_frame.setVisible(true)
    controller_frame.setVisible(true)
    Future {
      while(true) {
        slider_value = slider.getValue
        if(slider_value == -1)
          Thread.sleep(1)
        else if(slider_value == maximum_value + 1) {
          run_each_cycle_on_the_main_entity_thread_so_the_ticks_pass_by()
        } else {
          val desired_wait = 1000.0 / exp_value(slider_value)
          val millis = desired_wait.asInstanceOf[Int]
          val nanos = (desired_wait * 1000000).asInstanceOf[Int] % 1000000
          Future {
            //val abs_speed = (e : EntityWithSpeed) => Math.sqrt(e.u*e.u + e.v*e.v)
            //val max_speed = abs_speed(entities.filter(_.isInstanceOf[EntityWithSpeed])
            //  .asInstanceOf[Array[EntityWithSpeed]].maxBy(abs_speed))
            run_each_cycle_on_the_main_entity_thread_so_the_ticks_pass_by()
          }
          Thread.sleep(millis, nanos)
        }
      }
    }
    while(true) {
      Future {
        canvas.repaint()
      }
      Thread sleep 15
    }
  }

  private def run_each_cycle_on_the_main_entity_thread_so_the_ticks_pass_by() {
    entities.foreach(_.tick())
    ticks += 1
  }

  private def exp_value(i : Int) = Math.pow(10, i / 4).asInstanceOf[Int] * Array(1, 2, 4, 5)(i % 4)
  private def neat_value(i : Int) : String = i match {
      case a if a < 0 =>
        "-" + neat_value(-i)
      case a if a >= 1000000000 =>
        val left = (a / 100000000).toString
        left(0) + "." + left(1) + "G"
      case a if a >= 10000000 =>
        val left = (a / 1000000).toString
        left + "M"
      case a if a >= 1000000 =>
        val left = (a / 100000).toString
        left(0) + "." + left(1) + "M"
      case a  if a >= 10000 =>
        val left = (a / 1000).toString
        left + "k"
      case a if a >= 1000 =>
        val left = (a / 100).toString
        left(0) + "." + left(1) + "k"
      case a =>
        a.toString
  }
}
