trait EntityWithBounds extends EntityWithSpeed {
  var size = 0.0
  var bounds = (0.0, 0.0, 0.0, 0.0)
  override def tick() {
    super.tick()
    if(position.x + size/2 > bounds._3) {
      position.x = bounds._3 - size/2
      speed.x = -speed.x
    }
    if(position.x - size/2 < bounds._1) {
      position.x = bounds._1 + size/2
      speed.x = -speed.x
    }
    if(position.y + size/2 > bounds._4) {
      position.y = bounds._4 - size/2
      speed.y = -speed.y
    }
    if(position.y - size/2 < bounds._2) {
      position.y = bounds._2 + size/2
      speed.y = -speed.y
    }
    Controller.entities.foreach {
      case b : EntityWithBounds if b != this => interactWith(b)
      case _ =>
    }
  }

  def interactWith(a : EntityWithBounds) {
    val vec = position.dist(a.position)
    val tol = (size + a.size) / 2
    if(vec.len >= tol)
      return
    val norm_vec = vec.clone
    norm_vec.len = tol
    val avg = position + (vec * 0.5)
      position = avg + norm_vec
    a.position = avg - norm_vec
    val speeds = (speed.clone, a.speed.clone)
      speed = speeds._2
    a.speed = speeds._1
  }
}
