import java.awt.Graphics2D

class Sphere(the_position : Position, the_size : Double,
             the_bounds : (Double, Double, Double, Double), the_speed : Vector)
  extends EntityWithBounds with LivingEntity {
  bounds = the_bounds
  size = the_size
  position = the_position
  speed = the_speed

  def this() = this(new Position(0, 0), 0, (0, 0, 0, 0), new Vector(0, 0))

  def draw(g : Graphics2D) {
    val args = ((position.x - size/2).asInstanceOf[Int],
                (position.y - size/2).asInstanceOf[Int],
                        size.asInstanceOf[Int])
    g.fillOval(args._1, args._2, args._3, args._3)
  }

}
